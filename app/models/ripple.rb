class Ripple < ActiveRecord::Base
  validates :name, :message, presence: true
  validates :name, uniqueness: true
end
