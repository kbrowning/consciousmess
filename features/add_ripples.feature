Feature: Adding Ripples

  Should be able to add ripples with and without a website.

  Scenario: Sally adds a ripple that includes the optional website
  
    Given Sally has chosen to create a ripple
    When Sally decides to add a "website url" to the ripple
    Then Sally is able to edit the website field
    And Sally's ripple creates successfully

  Scenario: Sally adds a ripple that does not include the optional website

    Given Sally has chosen to create a ripple
    When Sally decides not to add a "website url" to the ripple
    Then Sally is able to leave the website field blank
    And Sally's ripple creates successfully
