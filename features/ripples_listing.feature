Feature: Ripples Listing

  Should be able to view ripples

  Scenario: Sally views the latest ripple

    Given Sally goes to the CONSCIOUS/mess website
    When Sally clicks "Show" on the top ripple
    Then Sally is taken to "that ripple's page"

  Scenario: Sally creates a ripple then views the ripples listing
 
    Given Sally has created a ripple
    When Sally goes to the "index page"
    Then Sally is able to click "Show"
    And Sally is taken to "her ripple's page"
