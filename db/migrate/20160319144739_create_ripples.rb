class CreateRipples < ActiveRecord::Migration
  def up
    create_table :ripples do |t|
      t.string :name
      t.string :url
      t.string :message

      t.timestamps
    end
   
    def change
      add_column :ripples, :created_at, :datetime
    end
  end
end
