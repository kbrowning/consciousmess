require 'test_helper'

class RipplesControllerTest < ActionController::TestCase
  setup do
    @ripple = ripples(:one)
    @update = {
      name: 'Lorem Ipsum',
      message: 'lorem ipsum dolor sit amet...'
    }
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ripples)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ripple" do
    assert_difference('Ripple.count') do
      post :create, ripple: @update
    end

    assert_redirected_to ripples_url
  end

  test "should show ripple" do
    get :show, id: @ripple
    assert_response :success
  end

  test "should update ripple" do
    patch :update, id: @ripple, ripple: @update

    assert_redirected_to ripples_url
  end
end
